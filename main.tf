# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.41.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  user_domain_name = "jalasoft"
  user_name        = var.user_name
  password         = var.user_password
  auth_url         = var.auth_url
  tenant_id        = var.tenant_id
  insecure         = true
  region           = "jala-hq"
}

resource "openstack_compute_keypair_v2" "key_pair" {
  name       = var.key_pair
  public_key = file("${var.ssh_key_file}")
}

resource "openstack_compute_secgroup_v2" "secgroup_1" {
  name        = "${var.security_group}"
  description = "My security group - Ivar Fuentes"
  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
  rule {
    from_port   = 3389
    to_port     = 3389
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
  rule {
    from_port   = 8080
    to_port     = 8080
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
  rule {
    from_port   = -1
    to_port     = -1
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }
}


# Create a dev server
resource "openstack_compute_instance_v2" "dev_server" {
  #count           = var.instance_count
  name            = "${var.instance_prefix}01"
  image_name      = var.image
  flavor_name     = var.flavor
  key_pair        = openstack_compute_keypair_v2.key_pair.name
  security_groups = [var.security_group]
  #security_groups = ["default", "${openstack_compute_secgroup_v2.secgroup_1.name}"]

  block_device {
    uuid                  = var.volume_id
    source_type           = "volume"
    volume_size           = 50
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = false
  }

  connection {
    user        = "ubuntu"
    host        = openstack_compute_instance_v2.dev_server.network[0].fixed_ip_v4
    type        = "ssh"
    private_key = file("${var.private_key_path}")


  }

  provisioner "remote-exec" {
    inline = [
      "wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -",
      "sudo sh -c 'echo deb https://pkg.jenkins.io/debian binary/ > /etc/apt/sources.list.d/jenkins.list'",
      "sudo apt-get -y update",
      "sudo apt-get -y install default-jre",
      "sudo apt-get -y install jenkins",
      "sudo systemctl status jenkins",
    ]
  }
}
