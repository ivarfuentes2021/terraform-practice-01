variable "user_name" {
  type      = string
  sensitive = true
}
variable "user_password" {
  type      = string
  sensitive = true
}

variable "image" {
  type    = string
  default = ""
}

variable "flavor" {
  type    = string
  default = ""
}

variable "ssh_key_file" {
  type    = string
  default = "~/.ssh/ivar_test.pub"
}

variable "instance_count" {
  default = 1
}

variable "instance_prefix" {
  type    = string
  default = "VMIVFU"
}

variable "key_pair" {
  type    = string
  default = ""
}
variable "security_group" {
  type    = string
  default = ""
}

variable "auth_url" {
  type    = string
  default = ""
}

variable "tenant_id" {
  type    = string
  default = ""
}

variable "volume_id" {
  type    = string
  default = ""
}
variable "private_key_path" {
  type    = string
  default = ""
}

